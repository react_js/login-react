import  React, { Component } from 'react'
import { Link } from 'react-router-dom'

export class Barra extends Component {
    render () {
        const { total } = this.props
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a className="navbar-brand">La bodega</a>
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0"></ul>
                    <form className="form-inline my-2 my-lg-0">
                        <button onClick={() => { this.props.change() } } type="button" className="btn btn-outline-primary">
                            <i className="fas fa-bars"></i>
                        </button>
                        <button onClick={() => { this.props.change() } } type="button" className="btn btn-primary">
                            <i className="fas fa-cart-arrow-down"></i> 
                            <span className="badge badge-light">{ total }</span>
                        </button>
                        <Link to='/' type="button" className="btn btn-outline-secondary">
                            <i className="fas fa-power-off"></i>
                        </Link>
                    </form>
                </div>
            </nav>
        )
    }
}