import  React from 'react'

export class Carrito extends React.Component {
    constructor (props) {
        super(props)
        this.state = { productos: this.props.products, totalCarrito: this.props.totalNeto }
    } 

    render () {
        return (
            <div className="row">
                <div className="col-md-12 mt-2">
                    <div className="card">
                        <div className="card-header">
                            <h3>Mi carrito de compras</h3>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="list-group">
                                        {
                                            this.state.productos.map((value, index) => {
                                                return (
                                                    <a  className="list-group-item list-group-item-action" key={index}>
                                                        <img 
                                                            src={`/img/${ value['producto']['img'] }`} 
                                                            alt="" 
                                                            width="50px" 
                                                            height="50px" 
                                                        />
                                                        <strong>{ value['producto']['nombre'] }</strong><br />
                                                        Unidades: { value['cantidad'] } <br />
                                                        SubTotal: $ { value['cantidad'] * value['producto']['precio'] }
                                                    </a>
                                                )
                                            })
                                        }
                                    </div>             
                                </div>
                                <div className="col-md-6">
                                    <strong>Total: ${ this.state.totalCarrito}</strong> <br /><br />
                                    <div className="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" onClick={() => { this.props.change() } } className="btn btn-primary">cancelar</button>
                                        <button type="button" onClick={() => { this.props.pagar() } }  className="btn btn-secondary">Pagar</button>
                                    </div>
                                </div>  
                            </div>      
                        </div>
                    </div>
                </div>
            </div> 
        )
    }
}