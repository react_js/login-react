import React from 'react'

class Detalle extends React.Component {
    render() {
        const { detalleProduct } = this.props
        return (
            <div className="row">
                <div className="col-md-12 mt-2">
                {
                detalleProduct.map(producto => {
                    return (
                    <div className="card" key={producto.nombre}>
                        <div className="card-header">
                            <h3>{ producto.nombre }</h3>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-12">
                                
                                    <div className="card mb-5">
                                        <div className="row no-gutters">
                                            <div className="col-md-4">
                                                <img src={`/img/${producto.img}`} className="card-img" alt="..." />
                                            </div>
                                            <div className="col-md-8">
                                                <div className="card-body">
                                                    <p className="card-text">Precio: $ {producto.precio}</p>
                                                    <p className="card-text">Unidades disponibles: {producto.cantidad}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <button
                                        type="button" 
                                            onClick={ this.props.handleClick }  
                                            className="btn btn-primary btn-sm">
                                            Regresar
                                        </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    )
                })
                }
                </div>
            </div>  
        )
    }
}

export class ListProducts extends React.Component {
    constructor (props) {
        super(props)
        this.state = { 
                productos: this.props.products, 
                inputSearch: '', 
                isDetalle: false, 
                detalle: []
            }
    }

    handleChange = (e) => {
        let auxLista      = [];
        this.setState({ productos: this.props.products })

        if ( e.target.value.trim().length > 0 ){
            auxLista = this.state.productos.filter(product => product.nombre.includes(e.target.value))
            this.setState({ productos: auxLista })
        }
    }   
    
    handleClick = () =>{
        this.setState({ detalle: [], isDetalle: false })
    }   

    render() {
        if(this.state.isDetalle){
            return <Detalle detalleProduct={this.state.detalle} handleClick={this.handleClick} />
        }
        return (
            <div className="row">
                <div className="col-md-12 mt-2">
                    <div className="card">
                        <div className="card-header">

                            <div className="row row-cols-1 row-cols-md-2">
                                <div className="col-mb-8">
                                <h3>Catalogo de productos</h3>
                                </div>
                                <div className="col-mb-3">
                                    <div className="input-group input-group-sm mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-sm">Buscar</span>
                                        </div>
                                        <input type="text" 
                                            className="form-control" 
                                            onKeyUp={ this.handleChange } 
                                            aria-label="Sizing example input" 
                                            aria-describedby="inputGroup-sizing-sm" 
                                            ref="inputSearch"
                                        />
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <div className="card-body">
                            <blockquote className="blockquote mb-0">
                                <div className="row row-cols-1 row-cols-md-4" >
                                    {
                                    this.state.productos.map(producto => {
                                        return (
                                        <div className="col mb-3" key={producto.nombre}>
                                            <div className="card">
                                                <img src={`/img/${producto.img}`} className="card-img-top" width="100%" height="150px" alt="nombre" />
                                                <div className="card-body">
                                                
                                                    <div className="row row-cols-1 row-cols-md-1">
                                                        <div className="col-md-12">
                                                            <h5 className="card-title">{producto.nombre}</h5>
                                                            <p className="card-text">Precio: ${producto.precio}.</p>
                                                            <p className="card-text">Unidades disponibles: {producto.cantidad}</p>
                                                            <form >
                                                                <div className="row row-cols-1 row-cols-md-3">
                                                                    <div className="col-mb-4">
                                                                        <button
                                                                            type="button" 
                                                                            onClick={() => { this.setState({ isDetalle: true, detalle: [producto] }) } }  
                                                                            className="btn btn-primary btn-sm">
                                                                            Ver mas
                                                                        </button>
                                                                    </div>
                                                                    <div className="col-mb-4">
                                                                        <button type="button" onClick={(e) => { this.props.SumCarrito(this.refs[producto.nombre].value, producto) } } className="btn btn-warning btn-sm">
                                                                            Agregar
                                                                        </button>
                                                                    </div>
                                                                    <div className="col-mb-4">
                                                                        <input 
                                                                            type="number" 
                                                                            name="cantidad"
                                                                            className="form-control" 
                                                                            min="0" 
                                                                            ref={producto.nombre}
                                                                            max={producto.cantidad} />
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        )
                                    })
                                    }
                                </div>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}