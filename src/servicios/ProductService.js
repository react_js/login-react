import React from 'react'

export class ProductService extends React.Component {
    constructor () {
        super()
        this.state = { 
            lista: [
                { "nombre": "Aguacate", "precio": 5, "cantidad": 35, "img": 'aguacate.jpg' },
                { "nombre": "Ajo", "precio": 2, "cantidad": 35, "img": 'ajo.jpg' },
                { "nombre": "Almendras", "precio": 4, "cantidad": 35, "img": 'almendras.jpg' },
                { "nombre": "Arandanos", "precio": 5, "cantidad": 35, "img": 'arandanos.jpg' },
                { "nombre": "brocoli", "precio": 5, "cantidad": 35, "img": 'brocoli.png' },
                { "nombre": "calabaza", "precio": 15, "cantidad": 35, "img": 'calabaza.jpg' },
                { "nombre": "canela", "precio": 5, "cantidad": 21, "img": 'canela.jpg' },
                { "nombre": "cebolla", "precio": 5, "cantidad": 35, "img": 'cebolla.jpg' },
                { "nombre": "fresa", "precio": 7, "cantidad": 35, "img": 'fresa.jpg' },
                { "nombre": "kiwi", "precio": 5, "cantidad": 45, "img": 'kiwi.jpg' },
                { "nombre": "limon", "precio": 3, "cantidad": 5, "img": 'limon.jpg' },
                { "nombre": "lychee", "precio": 5, "cantidad": 35, "img": 'lychee.jpg' },
                { "nombre": "maiz", "precio": 5, "cantidad": 15, "img": 'maiz.jpg' },
                { "nombre": "manzana", "precio": 10, "cantidad": 35, "img": 'manzana.jpg' },
                { "nombre": "naranja", "precio": 5, "cantidad": 35, "img": 'naranja.jpg' },
                { "nombre": "papa", "precio": 5, "cantidad": 35, "img": 'papa.jpg' },
                { "nombre": "pasta", "precio": 5, "cantidad": 31, "img": 'pasta.jpg' },
                { "nombre": "pimienta", "precio": 5, "cantidad": 25, "img": 'pimienta.jpg' },
                { "nombre": "repollo", "precio": 3, "cantidad": 35, "img": 'repollo.jpg' },
                { "nombre": "tomate", "precio": 2, "cantidad": 30, "img": 'tomate.jpg' },
                { "nombre": "zanahoria", "precio": 6, "cantidad": 35, "img": 'zanahoria.jpg' }
            ], 
            carrito: [],
            total: 0
        }
    }

    getProductos() {
        return this.state.lista
    }

    getCarrito() {
        return this.state.carrito
    }

    pagarCarrito = () => {
        this.setState({ carrito: [], total: 0 })
    }

    getTotalNeto(){
        var total = 0
        for (let index = 0; index < this.state.carrito.length; index++) {
          const element = this.state.carrito[index]
          total += (element.producto['precio'] * element.cantidad)
        }
        return total;
      }

    getTotal() {
        return this.state.total
    }

    setCarrito(cantidad, producto) {
        this.setState({ total: this.state.total + 1 })
        this.state.carrito.push({ 'producto': producto,'cantidad': cantidad });
        this.state.lista.find(v => v.nombre == producto['nombre']).cantidad = producto['cantidad'] - cantidad;
    }
}