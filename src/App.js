import React from 'react';
import { Switch, Route } from 'react-router-dom'

import { Login } from './pages/Login'
import { Productos } from './pages/Productos'
import { NotFound } from './pages/NotFound'

import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path='/' component={Login} />
          <Route exact path='/productos' component={Productos} />
          <Route component={NotFound} />
        </Switch>
      </div>
    );
  }
}

export default App;
