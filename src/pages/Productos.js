import  React from 'react'

import { Barra } from '../components/Barra'
import { ListProducts } from '../components/ListProducts'
import { ProductService } from '../servicios/ProductService'
import { Carrito } from '../components/Carrito'

export class Productos extends ProductService {

    constructor (props) {
        super(props)
        this.setState({ isCarrito: false, Render: '', products: this.getProductos() })
    }

    handleClick = (cantidad, producto) => {
        if(cantidad){
            this.setCarrito(cantidad, producto)
        }
    }

    _ChangeView = () =>{
        this.setState({ isCarrito: !this.state.isCarrito })
    }

    _PagarCarrito = () =>{
        this.setState({ isCarrito: !this.state.isCarrito })
        this.pagarCarrito()
    }

    _Render(){
        if(this.state.isCarrito){
            return  <Carrito 
                        products={this.getCarrito()} 
                        totalNeto={this.getTotalNeto()} 
                        change={ this._ChangeView } 
                        pagar={ this._PagarCarrito } /> 
        }else{
            return  <ListProducts products={this.state.lista} SumCarrito={this.handleClick} />
        }
    }

    render () {
        return (
            <div className="container">
                <Barra total={this.getTotal() } change={ this._ChangeView } />
                { this._Render() }
            </div>
        )
    }
}