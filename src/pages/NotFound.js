import React from 'react'
import { BackToHome } from '../components/BackToHome'

export const NotFound = () => (
	<div>
		<h1 className='title'>404!</h1>
		<p className='subtitle'>No existe la pagina</p>
		<BackToHome />
	</div>
)