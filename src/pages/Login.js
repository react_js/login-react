import React from 'react';
import { PropTypes } from 'prop-types'

export class Login extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  }

  constructor(props){
    super(props);

    this.state = {
        fields: {},
        errors: {},
        state: ''
    }
 }

  handleValidation(){
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      //Email
      if(!fields["email"]){
        formIsValid = false;
        errors["email"] = "El email no puede ser nulo";
      }

      if(typeof fields["email"] !== "undefined"){
          let lastAtPos = fields["email"].lastIndexOf('@');
          let lastDotPos = fields["email"].lastIndexOf('.');

          if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
            formIsValid = false;
            errors["email"] = "El email no es valido";
          }
      }  

     //password
     if(!fields["pass"]){
        formIsValid = false;
        errors["pass"] = "La contraseña no puede ser nula";
     }

     if(typeof fields["name"] !== "undefined"){
        if(!fields["pass"].match(/^[a-zA-Z]+$/)){
           formIsValid = false;
           errors["pass"] = "Only letters";
        }        
     }

    this.setState({errors: errors});
    return formIsValid;
}

contactSubmit(e){
     e.preventDefault();
     let fields = this.state.fields;
     let errors = {};

     if(this.handleValidation() && fields["email"] === 'demo@gmail.com' && fields["pass"] === 'secret' ){
      this.props.history.push('/productos')
     }else{
        this.setState({ state: 'Datos incorrectos' })
     }
 }

  handleChange(field, e){         
    let fields = this.state.fields;
    fields[field] = e.target.value;        
    this.setState({fields});
  }

  render() {
    return (
      <div className="login-page">
        <div className="form">
          <form onSubmit= {this.contactSubmit.bind(this)}>
            <h2>Iniciar sesión</h2>
            <label> <span style={{color: "red"}}>{this.state.state}</span></label>
            <label>
              Correo electrónico 
              <span style={{color: "red"}}>{this.state.errors["email"]}</span>
            </label>
            <input type="text" id="email-input" value={this.state.fields["email"]} onChange={this.handleChange.bind(this, "email")} name="email-grupo"/>
            <label>
              Contraseña 
              <span style={{color: "red"}}>{this.state.errors["pass"]}</span>
            </label>
            <input type="password" name="pass-grupo" onChange={this.handleChange.bind(this, "pass")} id="pass-input"/>
            
            <button>Redirect</button> 
          </form>
        </div>
      </div>
    );
  }
}